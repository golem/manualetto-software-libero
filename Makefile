## Reduced version of TEX makefile

### Select all raster ###
RAST_DIR = ./img
RASTF    = $(wildcard $(RAST_DIR)/*)

all: build/main.pdf

booklet: build/main.pdf
	@pdfbook --outfile build $<

build/%.pdf: %.tex $(RASTF) Makefile
	@mkdir -p build
	@git rev-parse HEAD | dd bs=1 count=10 > build/commit.tex
	@git log -1 --format=%ci | cut -d' ' -f1 > build/commit-date.tex
	@status=$$(git status --porcelain); if test "x$${status}" != x; then echo " dirty" >> build/commit.tex; fi
	@if [ -t 1 ]; then echo -e "\033[1;33m[Mashup      ]\033[0m main.tex"; else echo "Mashup"; fi
	@pdflatex -output-directory build -synctex=1 -interaction=nonstopmode $<
	@pdflatex -output-directory build -synctex=1 -interaction=nonstopmode $< > /dev/null

clean:
	rm -rf build
	rm -rf $(VEC_DIR)
